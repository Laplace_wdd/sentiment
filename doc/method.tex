\section{Methodology}
\label{sec:meth}
This section mainly talks about the details of the semi-supervised learning.
\subsection{Self-Training}
Given the seed data, self-training will give prediction on each test case. Given an input case $x$, different from the hard classifier that give prediction $f(x)\in \mathcal{C}$, in the self training classifier, the system output will be soft that also produces the probability for each label, where $f_s(x) = (y,p(y|x))\in \mathcal{C}\times[0,1]$ By choosing the classified data who has dominate probability over they other candidates as new training data. The training set will be enriched for the second run of training and testing. The self-training procedure runs iteratively until most data have been classified with high confidence(98\% in our experimental setting), The algorithm is described in Algorithm \ref{alg:strain}:\\
\begin{algorithm}[!h]
	\SetKwInOut{Input}{Input}
	\SetKwFunction{selftrain}{ALGORITHM}
	\SetKw{proc}{Procedure}
	\SetAlgoLined
	\Input \\
	{$Seeds~\mathcal{S}=\{(s_1,c_1),(s_2, c_2),\cdots,(s_m, c_m)\}$
	$Test~data~\mathcal{T}=\{x_1, x_2,\cdots, x_n\}$\\}
	\proc{\selftrain}{\\
		\While{$\frac{\sharp \mathcal{S}}{\sharp \mathcal{T}}<\sigma$}{
			Train a classifier: $f\leftarrow \mathcal{S}$\\
			\For{$x \in \mathcal{T}$}{
				\If {$f_p(x) = p(y|x) > \theta$}{
					$\mathcal{S}\leftarrow (x, y(x))$\\
				}
			}
		}
	}
\caption{\label{alg:strain}Self-Training}
\end{algorithm}

Where $\theta$ is the threshold for adding the test case into training data, which is 0.95 for positive cases and 0.7 for negative cases. $\sigma$ is the threshold for ending the iteration loop of learning, which controls the number of new training data. When the training data is large enough compared to the test data (in our experiment $\sigma=0.7$), the iteration will stop.

\subsection{Co-Training}
The assumption for co-training is there are at least two kinds of feature are uncorrelated. In our study, we assume the content features and meta feature are independent as they reflect two different aspects of citation. In the co-training approach, citation features are incorporated iteratively by their co-relation to the class. The co-relation is defined by a function with binary inputs $h(c,x)\sim\mathcal{C}\times\mathcal{X}\rightarrow[0,1]$, where $h$ can be interpreted as the conditional probability $p(c|x)$ of class $c$ given a fact that feature $x$ is seen in the case. $\mathcal{X}$ here is the feature space. This approach is called decision list, where each decision $x\rightarrow c$ are sorted by the co-relation function $h$. In addition, the final label of a given input is:
\begin{align}
	c_x = argmax_{x\in \mathcal{X},c\in\mathcal{C}}h(c,x)
\end{align}
Where the label of the input case is defined by its most ``confident'' feature. In practice, the co-relation function $h$ is defined by counting with Laplace smoothing:
\begin{align}
	h(c,x) = \frac{Count(c,x) + \alpha}{Count(x) + m\alpha}
\end{align}

Where $Count(c,x)$ is the number of co-occurrence of $(c,x)$ pair, $\alpha$ is the Laplace smoother, $m$ is the number of classes. In our case, $m=2$ because our task is binary classification. 
\\

Given a set of ``seed'' corpus, we first calculate the co-relation between features and classes. Then add these features into the feature set, and the labels are predicted by the logic in Equation (1).
\\
\begin{algorithm}[!h]
	\SetKwInOut{Input}{Input}
	\SetKwFunction{cotrain}{ALGORITHM}
	\SetKw{proc}{Procedure}
	\SetAlgoLined
	\Input \\
	{$Seed~Features:~\mathcal{S}=\{(s_1,c_1),(s_2, c_2),\cdots,(s_m, c_m)\}$
	$Test~data:~\mathcal{T}=\{t_1, t_2,\cdots, t_n\}$\\
	$Batch~Number:n=5$\\
	$Class~Number:m=2$\\}
	\proc{\selftrain}{\\
		\While{true}{
			1.Extract feature pairs from the current training set $\mathcal{S}$\\ 
			2.Distribute the features into $Set_{content}$ and $Set_{meta}$ by their types\\
			3.Count the occurrence of $Count(c,x)$ for each $(c,x)$ pair\\
			4.Use features in $Set_{content}$ to predict labels, using Equation (1)\\
			5.Add top $n\times m$ in $Set_{meta}$ with $\max_c h(c,x)>\theta$ into the $Set_{meta}$\\
			6.Use features in $Set_{meta}$ to predict labels, using Equation (1)\\
			7.Add top $n\times m$ in $Set_{content}$ with $\max_c h(c,x)>\theta$ into the $Content-S$\\
			8.$n=n+5$\\
			9.\If { $Set_{content}$ and $Set_{meta}$ are not change}{
			break
		}
		}
	}
\caption{\label{alg:fmi}Co-Training}
\end{algorithm}
After tuning the parameter, we select $\alpha=0.1$ and $\theta=0.8$ as our fixed parameter in the experiment.
