#! /usr/bin/python
from itertools import *
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	fea2id = {}
	p = re.compile('([01]),.*,\"(.*)\"')
	psp = re.compile('\W+')
	curid = 0
	seedF = open(Config.get('seed'), 'r')
	vecF = open(Config.get('vec2'), 'w')
	for l in seedF:
		m = p.match(l)
		if m:
			l2 = m.group(2) 
			wordlist = psp.split(l2)
			tmpdict = {}
			vecs = m.group(1) 
			for w in wordlist:
				fid = curid
				if w in fea2id:
					fid = fea2id[w]	
				else:
					fea2id[w] = fid
					curid += 1
				if fid in tmpdict:
					tmpdict[fid] += 1
				else:
					tmpdict[fid] = 1
			for idx in sorted(tmpdict.iterkeys()):
				vecs = vecs + ' ' + str(idx) + ':' + str(tmpdict[idx])
			vecF.write(vecs+'\n')		
	vecF.close()
	dictF = open(Config.get('fea2id_sent'), 'w')
	for key, value in sorted(fea2id.iteritems(), key=lambda (k,v): (v,k)):
		dictF.write(key + '\t' + str(value) + '\n')	
	dictF.close
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
