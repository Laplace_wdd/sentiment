#! /usr/bin/python
from itertools import *
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	f1 = open(sys.argv[1], 'r')
	f2 = open(sys.argv[2], 'r')
	f3 = open(sys.argv[3], 'r')
	f4 = open(sys.argv[4], 'r')
	lc = 0 
	s2 = set()
	s3 = set()
	s4 = set()
	for l1, l2, l3, l4 in izip(f1, f2, f3, f4):
		lc += 1
		if l1[0] != l2[0]:
			s2.add(lc)
		if l1[0] != l3[0]:
			s3.add(lc)
		if l1[0] != l4[0]:
			s4.add(lc)
	print s3-s4
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
