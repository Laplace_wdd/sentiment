#! /usr/bin/python
import sys, os, re, time
from itertools import *
from util_conf import Config
if __name__ == "__main__":
	start = time.clock()
	inF = open(sys.argv[1], 'r')
	cont = inF.read()
	cont = re.subn(' 4:(\d+) ', ' ', cont)[0]
	cont = re.subn(' 5:(\d+) ', ' ', cont)[0]
	cont = re.subn(' 6:(\d+) ', ' ', cont)[0]
#	cont = re.subn(' 0:(\d+) ', ' ', cont)[0]
#	cont = re.subn(' 1:(\d+) ', ' ', cont)[0]
#	cont = re.subn(' 2:(\d+) ', ' ', cont)[0]
	cont = re.subn(' 3:(\d+) ', ' ', cont)[0]
	oF = open(sys.argv[2], 'w')
	oF.write(cont)
	inF.close()
	oF.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
