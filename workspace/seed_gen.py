#! /usr/bin/python
import sys, os, re, time
from itertools import *
from util_conf import Config
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	senti = {}
	p = re.compile('type=(.*) len=.* word1=(.*) pos1=.* stemmed1=n priorpolarity=(.*)')
	polF = open(Config.get('polary'), 'r')
	for l in polF:
		m = p.match(l)
		if m:
			senti[m.group(2)] = m.group(1) + '_' + m.group(3)
	polF.close()
	inF = open(Config.get('citeF3'), 'r')
	cfF = open(Config.get('contentFeature'), 'r')
#	seedF = open(Config.get('seed'), 'w')
	seedF = open(Config.get('cfseed'), 'w')
	outF = open(Config.get('citeF5'), 'w')
	p1 = re.compile('(\S+)<neg>[\S+]')
	p2 = re.compile('(\S+)\[\S+\]')
	for l1, l2 in izip(inF, cfF):
		l2lst = l2.strip().split(' ')
		fealst = [0,0,0,0]
		for ele in l2lst:
			m = p1.match(ele)
			if m:
				tk = m.group(1)
				if tk in senti:
					modi = senti[tk]
					if modi == 'weaksubj_negative':
						fealst[0] += 1
					if modi == 'strongsubj_negative':
						fealst[1] += 1
					if modi == 'strongsubj_positive':
						fealst[2] += 1
					if modi == 'weaksubj_positive':
						fealst[3] += 1
				continue
			m = p2.match(ele)
			if m:
				tk = m.group(1)
				if tk in senti:
					modi = senti[tk]
					if modi == 'strongsubj_positive':
						fealst[0] += 1
					if modi == 'weaksubj_positive':
						fealst[1] += 1
					if modi == 'weaksubj_negative':
						fealst[2] += 1
					if modi == 'strongsubj_negative':
						fealst[3] += 1
		appstr = ','+str(fealst[0])+','+str(fealst[1])+','+str(fealst[2])+','+str(fealst[3])
		outF.write(appstr + l1)
		if fealst[0] > 0 and fealst[2] == 0 and fealst[3] == 0:
#			seedF.write('1'+appstr+l1)
			seedF.write(l2)
		if fealst[0] == 0 and fealst[1] == 0 and fealst[3] > 0:
#			seedF.write('0'+appstr+l1)
			seedF.write(l2)
	outF.close()
	seedF.close()
	inF.close()
	cfF.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
