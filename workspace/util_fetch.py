#!/usr/bin/env python
import sys, re
from util_conf import Config
#build config, the format of the config file is:
#<N>item</N><V>value</V>
class FetchInfo:

	@staticmethod
	def genCiteNet():
		cinet = {}
		citeNetworkF = open(Config.get('citeNetwork'), 'r')
		citp = re.compile('([A-Z]\d{2}-\d{4})\s==>\s([A-Z]\d{2}-\d{4})')
		for line in citeNetworkF:
			m = citp.match(line)
			if m:
				sou = m.group(1)
				sin = m.group(2)
				if sin in cinet:
					cinet[sin].append(sou)
				else:
					cinet[sin] = [sou]
		citeNetworkF.close()

	@staticmethod
	def genId2Author():
		id2au = {}
		authorF = open(Config.get('author'), 'r')
		citp = re.compile('\d+\t([A-Z]\d{2}-\d{4})\t(.*)\t.*')
		for line in authorF:
			m = citp.match(line)
			if m:
				auts = set()
				aulst = m.group(2).split(';')
				pid = m.group(1)
				for name in aulst:
					auts.add(name.strip())
				id2au[pid] = auts
		authorF.close()
		return id2au

	@staticmethod
	def genId2CiteNum():
		id2cn = {}
		inciteF = open(Config.get('incite'), 'r')
		citp = re.compile('(\d+)\t([A-Z]\d{2}-\d{4})\t.*')
		for line in inciteF:
			m = citp.match(line)
			if m:
				auts = set()
				pid = m.group(2)
				val = m.group(1)
				id2cn[pid] = val
		inciteF.close()
		return id2cn

	@staticmethod
	def genCoAutNet():
		conet = {}
		coauNetworkF = open(Config.get('coauthor'), 'r')
		coap = re.compile('(.*)\s==>\s(.*)')
		for line in coauNetworkF:
			m = coap.match(line)
			if m:
				sou = m.group(1)
				sin = m.group(2)
				if sou in conet:
					conet[sou].add(sin)
				else:
					conet[sou] = set([sin])
		coauNetworkF.close()
		return conet

