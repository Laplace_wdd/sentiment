#!/usr/bin/env python
import sys
import re
#build config, the format of the config file is:
#<N>item</N><V>value</V>
class Config:

	__conf = {}
	
	@staticmethod
	def load(confName):
		conf = open(confName, 'r')
		p = re.compile('<N>(\S+)</N><V>(\S+)</V>')
		for line in conf:
			match = p.match(line)
			if match:
				Config.__conf[match.group(1)] = match.group(2)
	
	@staticmethod
	def get(name):
		if name in Config.__conf:
			return Config.__conf[name]
		else:
			return '#none#'
