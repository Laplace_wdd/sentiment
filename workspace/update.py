#! /usr/bin/python
from itertools import *
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	tr = open(sys.argv[1], 'r')
	te = open(sys.argv[2], 'r')
	res = open(sys.argv[3], 'r')
	tro = open(sys.argv[4], 'w')
	teo = open(sys.argv[5], 'w')
	thr0 = float(sys.argv[6])
	thr1 = float(sys.argv[7])
	for l in tr:
		tro.write(l)
	res.readline()
	p1 = re.compile('2 (.*)')
	p2 = re.compile('([01]) (.*) (.*)')
	for l1, l2 in izip(te,res):
		m2 = p2.match(l2)
		s1 = float(m2.group(2))
		s2 = float(m2.group(3))
		if s1 > thr0 or s2 > thr1:
			m1 = p1.match(l1)
			tro.write(m2.group(1) + ' ' + m1.group(1) + '\n')
		else:
			teo.write(l1)
	tr.close()
	te.close()
	res.close()
	tro.close()
	teo.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
