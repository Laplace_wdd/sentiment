import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	inF = open(sys.argv[1], 'r')
	oF = open(sys.argv[2], 'w')
	psp = re.compile('\W+')
	pdep = re.compile('.*\(.*\)')
	for l in inF:
		l = l.strip()
		l = re.subn('<neg>', '', l)[0]
		wordlist = l.split(' ')
		outs = ''
		for w in wordlist:
#			if pdep.match(w):
#				continue
			outs = outs + w + ' '
		oF.write(outs.strip() + '\n')
	inF.close()
	oF.close()
