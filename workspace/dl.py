#! /usr/bin/python
from itertools import *
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	seedF = open(sys.argv[1], 'r')
	testF = open(sys.argv[2], 'r')
	predF = open(sys.argv[3], 'w')
	labelSet = set(['0','1'])
	trlst = []
	p = re.compile('([01]) (.*)')
	alpha = 1
	thr0 = 0.8
	thr1 = 0.8
	#init
	for l in seedF:
		m = p.match(l)
		fealst = m.group(2).split(' ')
		feaSet = set() 
		for feap in fealst:
			fp = feap.split(':')
			if fp[1] != '0':
				feaSet.add(fp[0])
		trlst.append((m.group(1), feaSet))
	p1 = re.compile('[01] (.*)')
	telst = []
	lcnt = 0
	for l in testF:
		lcnt += 1
		m = p1.match(l)
		fealst = m.group(1).split(' ')
		feaSet = set() 
		for feap in fealst:
			fp = feap.split(':')
			if fp[1] != '0':
				feaSet.add(fp[0])
		telst.append(feaSet)
	poslab = ['2' for n in range(lcnt)]
	for t in range(1,10):
		flc = {}
		fc = {}
		for (l,s) in trlst:
			for f in s:
				if f in flc:
					if l in flc[f]:
						flc[f][l] += 1
					else:
						flc[f][l] = 1.0
				else:
					flc[f] = {l:1.0}
				if f in fc:
					fc[f] += 1
				else:
					fc[f] = 1.0
		h = {}
		for f in flc:
			for l in flc[f]:
				score = (flc[f][l]+alpha)/(fc[f] + 2*alpha)
				if (l == '0' and score > thr0) or (l == '1' and score > thr1):
					h[f,l] = score
		for i in range(0,len(telst)):
			if poslab[i] != '2':continue
			cand = '2'
			ts = 0
			for f in telst[i]:
				for l in labelSet:
					if (f,l) in h:
						if h[f,l] > ts:
							ts = h[f,l]
							cand = l
			if cand != '2':
				trlst.append((cand, list(telst[i])))
			poslab[i] = cand 
	for l in poslab:
		if l == '2':
			predF.write('1\n')
		else:
			predF.write(l + '\n')
	seedF.close()
	testF.close()
	predF.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
