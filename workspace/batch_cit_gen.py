#! /usr/bin/python
import sys, os, re
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	Config.load(sys.argv[1])
	cinet = FetchInfo.genCiteNet()
	#===========get citation summary==========
	dataDir = Config.get('dataDir')
	txtDir = Config.get('txtDir')
	np = re.compile('([A-Z]\d{2}-\d{4}).CS')
	idp = re.compile('([A-Z]\d{2}-\d{4})')
	psp = re.compile('[^\w]+')
	batch = 1000
	count = 0
	fnum = 0
	citeF = open(Config.get('citeF') + '.' + str(fnum), 'w')
	for tmpName in os.listdir(dataDir):
		m = np.match(tmpName)
		if m:
			f = open(dataDir + tmpName, 'r')
			name = m.group(1)
			if name in cinet:
				idx = 0
				soulst = cinet[name]
				#print name + ':' + str(len(soulst))
				for line in f:
					if count == batch:
						citeF.close()
						count = 0
						fnum = fnum + 1
						citeF = open(Config.get('citeF') + '.' + str(fnum), 'w')
					line = line.strip('\n')
					if len(line) < 1 or line.startswith('----------'):
						continue
					idm = idp.match(line)
					if idm:
						citeF.write(',' + idm.group(1) + ',' + name + ',\"'+line+'\"\n')
						count = count + 1
						continue	
				#	print idx
					cits = re.subn('[^\w]+', '', line)[0]
					cits = cits.lower()
					if len(cits) < 20:
						continue
					cits = cits[len(cits) - 20:len(cits)]
					for sou in soulst:
						if not os.path.isfile(txtDir+sou + '.txt'):
							continue
						if len(soulst) == 1:
							citeF.write(',' + sou + ',' + name + ',\"'+line+'\"\n')
							count = count + 1
							break
						txtF = open(txtDir+sou+'.txt', 'r')
						txts = txtF.read()
						txtF.close()
						txts = re.subn('[^\w]+', '', txts)[0]
						if txts.find(cits) != -1:
							citeF.write(',' + sou + ',' + name + ',\"'+line+'\"\n')
							count = count + 1
							break
				f.close()
	citeF.close()
