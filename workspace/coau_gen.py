#! /usr/bin/python
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	id2au = FetchInfo.genId2Author()
	conet = FetchInfo.genCoAutNet()
	#===========get citation summary==========
	citeFin = open(Config.get('citeF2'), 'r')
	citeFout = open(Config.get('citeF3'), 'w')
	p = re.compile(',\d+,[01],([A-Z]\d{2}-\d{4}),([A-Z]\d{2}-\d{4}),\".*\"')
	for line in citeFin:
		m = p.match(line)
		if m:
			sou = m.group(1)
			sin = m.group(2)
			flag = '0'
			if sou in id2au:
				coset = set()
				soauset = id2au[sou]
				for au in soauset:
					if au in conet:
						coset.update(conet[au])
				if sin in id2au:
					siauset = id2au[sin]	
					if len(siauset&coset) > 0:
						flag = '1'
					citeFout.write(',' + flag + line)
	citeFout.close()
	citeFin.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
