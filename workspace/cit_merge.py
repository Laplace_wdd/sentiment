#! /usr/bin/python
import sys, os, re
from util_conf import Config
if __name__ == "__main__":
	Config.load(sys.argv[1])
	outF = open(sys.argv[2], 'w')
	header = 'citation.csv.'
	#===========get citation network==========
	sumDir = Config.get('sumDir')
	for i in range(0,16):
		f = open(sumDir+header+str(i), 'r')
		for line in f:
			outF.write(line)
		f.close()
	outF.close()

