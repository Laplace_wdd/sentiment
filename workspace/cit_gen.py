#! /usr/bin/python
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	cinet = FetchInfo.genCiteNet()
	#===========get citation summary==========
	dataDir = Config.get('dataDir')
	txtDir = Config.get('txtDir')
	np = re.compile('([A-Z]\d{2}-\d{4}).CS')
	idp = re.compile('([A-Z]\d{2}-\d{4})')
	psp = re.compile('[^\w]+')
	citeF = open(Config.get('citeF'), 'w')
	for tmpName in os.listdir(dataDir):
		m = np.match(tmpName)
		if m:
			f = open(dataDir + tmpName, 'r')
			name = m.group(1)
			if name in cinet:
				soulst = cinet[name]
				for line in f:
					line = line.strip('\n')
					if len(line) < 1 or line.startswith('----------'):
						continue
					idm = idp.match(line)
					if idm:
						citeF.write(',' + idm.group(1) + ',' + name + ',\"'+line+'\"\n')
						continue	
					cits = re.subn('[^\w]+', '', line)[0]
					cits = cits.lower()
					if len(cits) < 20:
						continue
					cits = cits[len(cits) - 20:len(cits)]
					for sou in soulst:
						if not os.path.isfile(txtDir+sou + '.txt'):
							continue
						if len(soulst) == 1:
							citeF.write(',' + sou + ',' + name + ',\"'+line+'\"\n')
							break
						txtF = open(txtDir+sou+'.txt', 'r')
						txts = txtF.read()
						txtF.close()
						txts = re.subn('[^\w]+', '', txts)[0]
						if txts.find(cits) != -1:
							citeF.write(',' + sou + ',' + name + ',\"'+line+'\"\n')
							break
				f.close()
	citeF.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
