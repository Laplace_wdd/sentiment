#! /usr/bin/python
import sys, os, re, time, nltk
from util_conf import Config
if __name__ == "__main__":
	selectedTag = set(['VB','VBD','VBG','VBN','VBP','VBZ','CC','JJ','JJR','JJS','RB','RBR','RBS'])
	start = time.clock()
	Config.load(sys.argv[1])
	#===========get citation summary==========
	fin = open(Config.get('citeF3'), 'r')
	fout = open(Config.get('citeF4'), 'w')
	p = re.compile('(,[01],\d+,[01],[A-Z]\d{2}-\d{4},[A-Z]\d{2}-\d{4},)\"(.*)\"')
	for line in fin:
		m = p.match(line)
		if m:
			meta = m.group(1)
			cont = m.group(2)
			tokens = nltk.word_tokenize(cont)
			tagged = nltk.pos_tag(tokens)
			vec = {}
			for pir in tagged:
				if pir[1] in selectedTag:
					if pir[0] in vec:
						vec[pir[0]] = vec[pir[0]] + 1
					else:
						vec[pir[0]] = 1
			strBuff = ''
			for term in vec:
				strBuff = strBuff + term + ':' + str(vec[term]) + ' '
			fout.write(meta+strBuff+',\"'+cont+'\"\n')
	fout.close()
	fin.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
