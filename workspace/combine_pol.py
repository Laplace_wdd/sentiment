#! /usr/bin/python
from itertools import *
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	vecF = open(Config.get('vec1'), 'w')
	fea2id = {'#SP#':3, '#WP#':4, '#WN#':5, '#SN#':6, '#COAUTHOR#':0,'#CITATION#':1,'#SELF#':2}
	p1 = re.compile(',(\d+),(\d+),(\d+),(\d+),([01]),(\d+),([01]),.*')
	curid = 7
	for l1, l2 in izip(open(Config.get('citeF5')), open(Config.get('contentFeature'))):
		m1 = p1.match(l1)
		vecs = ' 0:' + m1.group(5) + ' 1:' + m1.group(6) + ' 2:' + m1.group(7) + ' 3:' + m1.group(1) + ' 4:' + m1.group(2) + ' 5:' + m1.group(3) + ' 6:' + m1.group(4)
		l2 = l2.strip()
		wordlist = l2.split(' ')
		tmpdict = {}
		for w in wordlist:
			fid = curid
			if w in fea2id:
				fid = fea2id[w]	
			else:
				fea2id[w] = fid
				curid += 1
			if fid in tmpdict:
				tmpdict[fid] += 1
			else:
				tmpdict[fid] = 1
		for idx in sorted(tmpdict.iterkeys()):
			vecs = vecs + ' ' + str(idx) + ':' + str(tmpdict[idx])
		vecF.write(vecs+'\n')		
	vecF.close()
	dictF = open(Config.get('fea2id_pol'), 'w')
	for key, value in sorted(fea2id.iteritems(), key=lambda (k,v): (v,k)):
		dictF.write(key + '\t' + str(value) + '\n')	
	dictF.close
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
