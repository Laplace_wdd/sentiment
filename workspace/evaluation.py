#! /usr/bin/python
from itertools import *
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	f1 = open(sys.argv[1], 'r')
	f2 = open(sys.argv[2], 'r')
	tp0 = .0
	tp1 = .0
	fp0 = .0
	fp1 = .0
	fn0 = .0
	fn1 = .0
	n0 = .0
	n1 = .0
	corr = .0
	for l1, l2 in izip(f1, f2):
		if l1[0] == '0' and l2[0] == '0':
			corr += 1
			n0 += 1
			tp0 += 1
		if l1[0] == '0' and l2[0] == '1':
			n1 += 1
			fn0 += 1
			fp1 += 1
		if l1[0] == '1' and l2[0] == '0':
			n0 += 1
			fp0 += 1
			fn1 += 1
		if l1[0] == '1' and l2[0] == '1':
			corr += 1
			n1 += 1
			tp1 += 1 
	miP = (n0*tp0/(tp0+fp0)+n1*tp1/(tp1+fp1))/(n0+n1) 
	maP = (tp0/(tp0+fp0)+tp1/(tp1+fp1))/2 
	miR = (n0*tp0/(tp0+fn0)+n1*tp1/(tp1+fn1))/(n0+n1) 
	maR = (tp0/(tp0+fn0)+tp1/(tp1+fn1))/2 
	maF = 2*maP*maR/(maP+maR)
	miF = 2*miP*miR/(miP+miR)
	acc = corr/(n0+n1)
	print 'miP:' + str(miP) 
	print 'maP:' + str(maP) 
	print 'miR:' + str(miR) 
	print 'maR:' + str(maR) 
	print 'miF:' + str(miF) 
	print 'maF:' + str(maF) 
	print 'Acc:' + str(acc) 
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
