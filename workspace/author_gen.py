#! /usr/bin/python
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	#===========get citation summary==========
	id2au = FetchInfo.genId2Author()
	citeFin = open(Config.get('citeF'), 'r')
	citeFout = open(Config.get('citeF1'), 'w')
	p = re.compile(',([A-Z]\d{2}-\d{4}),([A-Z]\d{2}-\d{4}),\".*\"')
	for line in citeFin:
		m = p.match(line)
		if m:
			sou = m.group(1)
			sin = m.group(2)
			if sou in id2au:
				s1 = id2au[sou]
				s2 = id2au[sin]
				flag = '0'
				if len(s1&s2) > 0:
					flag = '1'
				citeFout.write(',' + flag + line)
	citeFout.close()
	citeFin.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
