#! /usr/bin/python
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	id2cn = FetchInfo.genId2CiteNum() 
	#===========get citation summary==========
	citeFin = open(Config.get('citeF1'), 'r')
	citeFout = open(Config.get('citeF2'), 'w')
	p = re.compile(',[01],[A-Z]\d{2}-\d{4},([A-Z]\d{2}-\d{4}),\".*\"')
	for line in citeFin:
		m = p.match(line)
		if m:
			sin = m.group(1)
			if sin in id2cn:
				citeFout.write(',' + id2cn[sin] + line)
	citeFout.close()
	citeFin.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
