#! /usr/bin/python
import sys, os, re, time
from util_conf import Config
from util_fetch import FetchInfo
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	psp = re.compile('\w+')
	in_f = open(Config.get('citeF'),'r')
	s_f = open(Config.get('sentence'),'w')
	p = re.compile(',[A-Z]\d{2}-\d{4},[A-Z]\d{2}-\d{4},\"(.*)\"')
	f_f = open(Config.get('citeFF'), 'w')
	docl = {}
	for l in in_f:
		m = p.match(l)
		if m:
			sent = m.group(1).split('\t').pop()
			leng = len(psp.split(sent))	
			if leng in docl:
				docl[leng] = docl[leng] + 1 
			else:
				docl[leng] = 1
			if leng > 80:continue
			f_f.write(l)
			s_f.write("%%%%" + sent + '\n')
	f_f.close()
	s_f.close()
	c_f = open(sys.argv[2], 'w')
	rklst = []
	for l in docl:
		rklst.append((l, docl[l]))
	rklst.sort()
	c_f.write(str(rklst[0][0]) + ' ' + str(rklst[0][1]) + '\n')
	stat = [rklst[0][1]]
	for i in range(1,len(rklst)):
		tst = stat[i-1] + rklst[i][1]
		stat.append(tst)
		c_f.write(str(rklst[i][0]) + ' ' + str(tst) + '\n')
	c_f.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
