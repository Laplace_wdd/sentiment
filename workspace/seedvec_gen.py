#! /usr/bin/python
import sys, os, re, time
from itertools import *
from util_conf import Config
if __name__ == "__main__":
	start = time.clock()
	Config.load(sys.argv[1])
	senti = {}
	p = re.compile('type=(.*) len=.* word1=(.*) pos1=.* stemmed1=n priorpolarity=(.*)')
	polF = open(Config.get('polary'), 'r')
	for l in polF:
		m = p.match(l)
		if m:
			senti[m.group(2)] = m.group(1) + '_' + m.group(3)
	polF.close()
	cfF = open(Config.get('contentFeature'), 'r')
	vecF = open(Config.get('vec1'), 'r')
	seedvF = open(Config.get('seedv'), 'w')
	testvF = open(Config.get('testv'), 'w')
	p1 = re.compile('(\S+)<neg>[\S+]')
	p2 = re.compile('(\S+)\[\S+\]')
	for l1, l2 in izip(cfF, vecF):
		l1lst = l1.strip().split(' ')
		fealst = [0,0,0,0]
		for ele in l1lst:
			m = p1.match(ele)
			if m:
				tk = m.group(1)
				if tk in senti:
					modi = senti[tk]
					if modi == 'weaksubj_negative':
						fealst[0] += 1
					if modi == 'strongsubj_negative':
						fealst[1] += 1
					if modi == 'strongsubj_positive':
						fealst[2] += 1
					if modi == 'weaksubj_positive':
						fealst[3] += 1
				continue
			m = p2.match(ele)
			if m:
				tk = m.group(1)
				if tk in senti:
					modi = senti[tk]
					if modi == 'strongsubj_positive':
						fealst[0] += 1
					if modi == 'weaksubj_positive':
						fealst[1] += 1
					if modi == 'weaksubj_negative':
						fealst[2] += 1
					if modi == 'strongsubj_negative':
						fealst[3] += 1
		if fealst[0] > 0 and fealst[2] == 0 and fealst[3] == 0:
			seedvF.write('1'+ l2)
			continue
		if fealst[0] == 0 and fealst[1] == 0 and fealst[3] > 0:
			seedvF.write('0'+ l2)
			continue
		testvF.write('2' + l2)
	cfF.close()
	vecF.close()
	seedvF.close()
	testvF.close()
	end = time.clock();
	print 'Processing Time:' + str((end - start)) + 'sec'
