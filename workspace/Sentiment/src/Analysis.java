import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.semgraph.SemanticGraphCoreAnnotations.BasicDependenciesAnnotation;
import edu.stanford.nlp.trees.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;
import java.io.*;
import java.util.*;
 
public class Analysis {
         public static void main(String[] args) throws IOException {
                   // creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution
             Properties props = new Properties();
             props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
             StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
             String senN = "/Users/dqwang/Study/my_codes/research/sentiment/workspace/data/sen.txt";
             String outN = "/Users/dqwang/Study/my_codes/research/sentiment/workspace/data/contF.txt";
             Scanner lineScanner = new Scanner(new File(senN));
             BufferedWriter outF = new BufferedWriter(new FileWriter(new File(outN)));
             Integer cnt = 0;
             while(lineScanner.hasNext()){

	             // read some text in the text variable
	             String text = lineScanner.nextLine();
	             if (!text.startsWith("%%%%"))continue;	         
	             text = text.substring(4, text.length());
	             // create an empty Annotation just with the given text
	             Annotation document = new Annotation(text);
	             
	             // run all Annotators on this text
	             pipeline.annotate(document);
	             
	             // these are all the sentences in this document
	             // a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
	             List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	 	             
	             for(CoreMap sentence: sentences) {
	            	// traversing the words in the current sentence
		               SemanticGraph dependencies = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
		               List<SemanticGraphEdge>lst = dependencies.findAllRelns(EnglishGrammaticalRelations.NEGATION_MODIFIER);
		             //  List<SemanticGraphEdge>lst = dependencies.edgeListSorted();
		               HashSet<Integer> negidx = new HashSet<Integer>();
		               for(SemanticGraphEdge e:lst) {
		            	   IndexedWord dep = e.getDependent();
		            	   IndexedWord gov = e.getGovernor();
		            	   negidx.add(gov.index());
		            	   Collection<IndexedWord> sibl = dependencies.getSiblings(dep);
		            	   for(IndexedWord iw:sibl){
		            		   SemanticGraphEdge ee = dependencies.getEdge(gov, iw);
		            		   if (ee.getRelation() == EnglishGrammaticalRelations.ADJECTIVAL_MODIFIER||
		            				   ee.getRelation() == EnglishGrammaticalRelations.ADVERBIAL_MODIFIER
		            				   ){
		            			   negidx.add(iw.index());
		            		   }
		            	   }
		               }
		               // a CoreLabel is a CoreMap with additional token-specific methods
		               for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
		                 // this is the text of the token
		                 String word = token.get(TextAnnotation.class);
		                 if (negidx.contains(token.index())){
		                	 word = word+"<neg>";
		                 }	                 
		                 // this is the POS tag of the token
		                 String pos = token.get(PartOfSpeechAnnotation.class);
		                 // this is the NER label of the token
		                 String ostr = word+"["+pos+"] ";
		                 outF.write(ostr);
		                 //System.out.print(ostr);
		               }
		               List<SemanticGraphEdge>alllst = dependencies.edgeListSorted();
		               for(SemanticGraphEdge e:alllst) {
		            	   String ostr = e.toString()+"("+e.getSource().word()+"_"+e.getTarget().word()+") ";
		            	   //System.out.print(ostr);
		            	   outF.write(ostr);
		               }
		               //System.out.println();
	             }
	             System.out.println(cnt++);
	             outF.write('\n');
             }
             lineScanner.close();
             outF.close();
         }
}